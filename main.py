import pygame
from pygame.locals import *
import time

pygame.init()
screen = pygame.display.set_mode((300, 300))
pygame.display.set_caption('Tic Tac Toe')
Color_line=(255,0,0)
running = True
BLUE=(192,192,192)
red=(0,0,0)
white=(255,255,255)
y=0
w=100
h=100
for i in range(0,3):
    x=0
    for j in range(0,3):
        pygame.draw.rect(screen,BLUE,(x,y,w,h))
        x+=100
    y+=100
pygame.draw.line(screen,Color_line,(100,0),(100,300))
pygame.draw.line(screen,Color_line,(200,0),(200,300))
pygame.draw.line(screen,Color_line,(0,100),(300,100))
pygame.draw.line(screen,Color_line,(0,200),(300,200))
count=0

team_one={'left':[],'right':[]}
team_two={'left':[],'right':[]}

def check_winning_team(list_of_co_ordinates):
    left_co_ordinates=list_of_co_ordinates['left']
    right_co_ordinates=list_of_co_ordinates['right']
    print("entry",len(left_co_ordinates))
    if len(left_co_ordinates)>=3:
        left_uniue_values=list(set(left_co_ordinates))
        right_uniue_values=list(set(right_co_ordinates))
        #for vertical
        for left_value in left_uniue_values:
            total_counts_left=left_co_ordinates.count(left_value)
            if total_counts_left==3:
                return total_counts_left
                # pygame.quit()
                # quit()
        #for horizontal
        for right_value in right_uniue_values:
            total_counts=right_co_ordinates.count(right_value)
            if total_counts==3:
                return total_counts
        #for cross
        count_cross_value_left=0
        count_cross_value_right=0
        for index in range(len(left_co_ordinates)):
            sum_of_index=left_co_ordinates[index] + right_co_ordinates[index]
            if sum_of_index==2:
                count_cross_value_left+=1
                if count_cross_value_left==3:
                    return count_cross_value_left
            if left_co_ordinates[index]==right_co_ordinates[index]:
                count_cross_value_right+=1
                if count_cross_value_right==3:
                    return count_cross_value_right
def game_loop():
    running = True
    count=0
    font = pygame.font.SysFont("comicsansms", 30)
    text_team_one = font.render("Team One Won", True, (0, 128, 0))
    text_team_two = font.render("Team Two Won", True, (0, 128, 0))
    while running:
        for event in pygame.event.get():
            if event.type == KEYDOWN:
                if event.key == K_ESCAPE:
                    running = False
            elif event.type == QUIT:
                running = False
            elif event.type == MOUSEBUTTONDOWN:
                print("down")
                x_position,y_position=pygame.mouse.get_pos()
                y=0
                w=100
                h=100
                for i in range(0,3):
                    x=0
                    for j in range(0,3):
                        if x_position>=x and y_position>=y and x_position<=(x+100) and y_position<=(y+100):
                            count+=1
                            print(count)
                            if count%2==0:
                                #team_one.append([j,i])
                                team_one['left'].append(j)
                                team_one['right'].append(i)
                                img=pygame.image.load(r'x.png')
                                #condition for winning
                                screen.blit(img,(x,y))
                                total_counts=check_winning_team(team_one)
                                if total_counts==3:
                                    print("You won ")
                                    screen.fill(white)
                                    screen.blit(text_team_two,(30, 80))

                            else:
                                #team_two.append([j,i])
                                team_two['left'].append(j)
                                team_two['right'].append(i)
                                img=pygame.image.load(r'0.png')
                                screen.blit(img,(x,y))
                                total_counts=check_winning_team(team_two)
                                if total_counts==3:
                                    print("You won ")
                                    screen.fill(white)
                                    screen.blit(text_team_one,(30,80))

                        x+=100
                    y+=100
        pygame.display.flip()

game_loop()
